package com.mycompany.uts_soal2;

/**
 *
 * @author Rochana
 */

public class Orang {
    public String nama;
    private final String NIK;
    
    // constructor
    public Orang(String nama, String NIK){
        this.nama = nama;
        this.NIK = NIK;
    }
    
    
    
    
    
    
    
    
    // method mendapatkan NO KK
    private String getKK(){
        String NoKK = "";
        for (int i=0;i<7;i++){
            NoKK = NoKK + NIK.charAt(i);
        }
        return NoKK;
    }
    
    // method mendapatkan status
    private String getStatus(){
        String status = "";
        char kodeStatus = NIK.charAt(13);
        if (kodeStatus == '0'){
            status = "Kepala keluarga";
        }
        else if (kodeStatus == '1'){
            status = "Istri";
        }
        if (kodeStatus == '2'){
            status = "Anak";
        }
        return status;
    }
    
    // method mencek hubungan keluarga
    private boolean isRelated(Orang o1){
        return this.getKK().equals(o1.getKK());
    }
    
    // method mengembalikan status
    public String getRelation(Orang o1){
        String relation = "";
        if (this.isRelated(o1)){
            relation = relation + this.nama + ": "+this.getStatus()+"\n"+
                    o1.nama + ": "+o1.getStatus();
        }
        else {
            relation = relation + this.nama +" dan "+o1.nama+" tidak ada hubungan";
        }
        return relation+"\n"+"====================";
    }
    
}
