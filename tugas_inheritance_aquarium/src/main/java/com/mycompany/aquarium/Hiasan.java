/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.aquarium;

public class Hiasan extends Item{
    
    public Hiasan(String nama, int harga){
        this.harga = harga;
        this.nama = nama;
    }
    
    public int dailyService(){
        int cost = (int)(0.1*harga);
        return -cost;
    }

    
    
}
