package com.mycompany.latihan_polimorfism;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // make object with child class type
        Student siswa = new Student("rian", "rian@gmail", 83.5, 2021);
        Teacher guru = new Teacher("rochana", "rochana@gmail", 3, 1);
        System.out.println("Membuat object menggunakan child class Student dan Teacher");
        System.out.println(siswa);
        System.out.println(guru);
        
        // make object with parent class type
        Person siswa2 = new Student("rian", "rian@gmail", 83.5, 2021);
        Person guru2 = new Teacher("rochana", "rochana@gmail", 3, 1);
        System.out.println("Membuat object menggunakan parent class Person");
        System.out.println(siswa2);
        System.out.println(guru2);

    }
    
}
