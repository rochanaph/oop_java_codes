
package com.mycompany.latihan_polimorfism;

public class Student extends Person{
    double grade;
    int year;
    
    public Student(String name, String email, double grade, int year){
        this.name = name;
        this.email = email;
        this.grade = grade;
        this.year = year;
    }
    
    @Override
    public String toString(){
        return "Nama: "+name+"\n"+
                "Email: "+email+"\n"+
                "Nilai: "+grade+"\n"+
                "Tahun masuk: "+year+"\n";
    }
}
