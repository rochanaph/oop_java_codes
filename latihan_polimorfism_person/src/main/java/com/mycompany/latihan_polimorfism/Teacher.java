
package com.mycompany.latihan_polimorfism;

public class Teacher extends Person{
    int level;
    int yearsOfExp;
    
    public Teacher(String name, String email, int level, int yearsOfExp){
        this.name = name;
        this.email = email;
        this.level = level;
        this.yearsOfExp = yearsOfExp;
    }
    
    @Override
    public String toString(){
        return "Nama: "+name+"\n"+
               "Email: "+email+"\n"+
               "Level: "+level+"\n"+
               "Tahun Pengalaman: "+yearsOfExp+"\n";
    }
    
    
    
}
