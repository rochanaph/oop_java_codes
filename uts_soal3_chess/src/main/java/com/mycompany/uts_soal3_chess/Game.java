/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.uts_soal3_chess;

/**
 *
 * @author Rochana
 */
public class Game {
    
    Piece [][] board;
    
    // constructor
    // membuat board kosong
    public Game(){
        board = new Piece[8][8];
    }
    
    public Piece getPiece(Position position){
        return board[position.x][position.y];
    }
    
    public void move(Piece piece, Position newPosition){
        Position oldPosition = piece.position;
        if(piece.isInBoard(newPosition) && piece.isOnRule(newPosition) && piece.isEatRule(newPosition, getPiece(newPosition))){
            board[oldPosition.x][oldPosition.y] = null;
            board[newPosition.x][newPosition.y] = piece;
            piece.setPosition(newPosition);
            System.out.println("SUCCESS");
        }else{
            System.out.println("FAIL");
        }
    }
    
    public void setPiece(Piece piece, Position position){
        board[position.x][position.y] = piece;
        piece.setPosition(position);
    }
    
}
