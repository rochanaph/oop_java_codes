/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.uts_soal3_chess;

/**
 *
 * @author Rochana
 */
public class Piece {
    Position position;
    String color;
   
    public boolean isInBoard(Position newPosition){
        return newPosition.x>0 && newPosition.y>0
           && newPosition.x<8 && newPosition.y<8;
    }
    
    public boolean isOnRule(Position newPosition){
        return true;
    }
    
    public boolean isEatRule(Position newPosition, Piece pieceInNewPos){
        return true;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
    
}
