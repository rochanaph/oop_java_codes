/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.uts_soal3_chess;

/**
 *
 * @author Rochana
 */
public class Pawn extends Piece{
    
    public Pawn(String col){
        color = col;
    }
    
    @Override
    public boolean isOnRule(Position newPosition){
        if(color.equals("Putih")){
            if (newPosition.x == this.position.x 
                && newPosition.y == this.position.y+1){
                return true;
            }
        }else{
            if (newPosition.x == this.position.x 
                && newPosition.y == this.position.y-1){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isEatRule(Position newPosition, Piece pieceInNewPos) {
        return pieceInNewPos == null;
    }
}
