/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.uts_soal3_chess;

/**
 *
 * @author Rochana
 */
public class Position {
    int x;
    int y;
    
    // constructor
    public Position(int x, int y){
        this.x = x;
        this.y = y;
    }
    
}
