/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.uts_soal3_chess;

/**
 *
 * @author Rochana
 */
public class Knight extends Piece{
    
    public Knight(String col){
        color = col;
    }
   
    @Override
    public boolean isOnRule(Position newPosition){
        
        if (newPosition.x == this.position.x+1 
            && newPosition.y == this.position.y+2){
                return true;
        }
        else if (newPosition.x == this.position.x-1 
            && newPosition.y == this.position.y+2){
                return true;
        }
        else if (newPosition.x == this.position.x+2 
            && newPosition.y == this.position.y+1){
            return true;
        }
        else if (newPosition.x == this.position.x-2 
            && newPosition.y == this.position.y+1){
            return true;
        }
        else {
            return false;
        }
        
    }
    
    @Override
    public boolean isEatRule(Position newPosition, Piece pieceInNewPos) {
        return pieceInNewPos == null || !pieceInNewPos.color.equals(this.color);
    }
}
