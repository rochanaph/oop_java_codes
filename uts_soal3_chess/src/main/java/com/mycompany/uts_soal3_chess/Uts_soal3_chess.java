/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.uts_soal3_chess;

/**
 *
 * @author Rochana
 */
public class Uts_soal3_chess {

    public static void main(String[] args) {
        
        // 2 knight dan 3 pawn
        Knight kuda1 = new Knight("Putih");
        Knight kuda2 = new Knight("Hitam");
        Pawn pion1 = new Pawn("Hitam");
        Pawn pion2 = new Pawn("Putih");
        Pawn pion3 = new Pawn("Putih");
        
        // buat papan catur baru
        Game game = new Game();
        // letakkan knight di (2,1) dan (3,3)
        game.setPiece(kuda1, new Position(1,0)); 
        game.setPiece(kuda2, new Position(2,2)); // posisi kuda hitam bisa dimakan kuda putih
        // letakkan pawn di (4,4)(4,5)(5,5)
        game.setPiece(pion1, new Position(3,3));
        game.setPiece(pion2, new Position(3,4));
        game.setPiece(pion3, new Position(4,4));
        
        game.move(kuda1, new Position(2,2)); // output SUCCESS, kuda putih berhasil bergerak ke posisi baru dan memakan kuda hitam
        game.move(pion3, new Position(4,5)); // output SUCCESS, pion putih bergerak 1 langkah
        game.move(pion1, new Position(3,4)); // output FAIL, pion putih gagal bergerak karena ada pion lain di depannya (pion2)
    }
}
