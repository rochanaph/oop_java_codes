/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.latihan_interface_caravan;

/**
 *
 * @author Rochana
 */
public class Latihan_interface_caravan {

    public static void main(String[] args) {
        // caravan
        Position start = new Position(1,1);
        Caravan trailer = new Caravan(start, 5);
        System.out.println("Can fit? "+trailer.canFit(4));
        System.out.println("Can move? "+trailer.canMove());
        if (trailer.canMove()){
            trailer.move(20);
            System.out.println("Current position: "+trailer.location);
        }
        
        // buat interface untuk tipe robotable
        // buat object untuk contoh transformer
        Transformer bumblebee = new Transformer("Fire", 3);
        if (bumblebee.canFight()){
            System.out.println(bumblebee);
        }
    }
}
