/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan_interface_caravan;

/**
 *
 * @author Rochana
 */
public class Caravan implements Habitable, Movable{
    Position location;
    int max;
    
    Caravan (Position location, int max){
        this.location = location;
        this.max = max;
    }
    
    @Override
    public boolean canFit(int inhabitants){
        return inhabitants <= max ;
    }
    @Override
    public boolean canMove(){
        return (location.x > 0 && location.y > 0) &&
                (location.x <= 100 && location.y <= 100);
    }
    @Override
    public void move(int distance){
        location.x = location.x + distance;
        location.y = location.y + distance;
    }
    
}
