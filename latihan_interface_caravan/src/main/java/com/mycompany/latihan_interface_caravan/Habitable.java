/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.latihan_interface_caravan;

/**
 *
 * @author Rochana
 */
public interface Habitable {
    
    public abstract boolean canFit(int inhabitants);
    
}
