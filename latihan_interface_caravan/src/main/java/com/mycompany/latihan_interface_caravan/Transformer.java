/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan_interface_caravan;

/**
 *
 * @author Rochana
 */
public class Transformer implements Robotable{
    String ability;
    int power;
    
    Transformer(String ability, int power){
        this.ability = ability;
        this.power = power;
    }
    
    @Override
    public boolean canFight(){
        return (power > 0);
    }
    
    @Override
    public String toString(){
        String res = "";
        for (int i = 0; i <= power; i++){
            res += ability + " !! ";
        }
        return res;
    }
    
}
