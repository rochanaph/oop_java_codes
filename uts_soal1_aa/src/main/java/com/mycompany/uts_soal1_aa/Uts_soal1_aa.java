/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.uts_soal1_aa;
import java.util.Scanner;

/**
 *
 * @author Rochana
 */
public class Uts_soal1_aa {
    public static void main(String[] args) {
        // Pegawai object_name = new Pegawai(nama, divisi, posisi)
        Pegawai a = new Pegawai("Rayendra", "Back-end","intern");
        Pegawai b = new Pegawai("Rahul", "Back-end","programmer");
        Pegawai c = new Pegawai("Niki", "Front-end","intern");
        Pegawai d = new Pegawai("Yahya", "Back-end","programmer");
        
        // Object baru e: nama Ailsa, divisi Front-end, posisi Programmer
        // Input dari user menggunakan scanner
        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Masukkan nama");
        String nama = myObj.nextLine();  // Read user input
        System.out.println("Masukkan divisi");
        String divisi = myObj.nextLine();
        System.out.println("Masukkan posisi");
        String posisi = myObj.nextLine();
        System.out.println(" ");
        
        Pegawai e = new Pegawai(nama, divisi, posisi);
        System.out.println("=====Daftar gaji pegawai=====");
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
    }
}
