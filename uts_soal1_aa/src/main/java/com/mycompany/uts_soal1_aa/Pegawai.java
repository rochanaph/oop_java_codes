/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.uts_soal1_aa;

/**
 *
 * @author Rochana
 */
public class Pegawai {
    String nama;
    String divisi;
    String posisi;
    double gaji;
    
    public Pegawai(String nama, String divisi, String posisi){
        this.nama = nama;
        this.divisi = divisi;
        this.posisi = posisi;
    }
    
    public double getSalary(){
        if (divisi.equals("Front-end")){
            if (posisi.equals("intern")){
                gaji = 600000;
            }
            if (posisi.equals("programmer")){
                gaji = 3000000;
            }
        }
        else if (divisi.equals("Back-end")){
            if (posisi.equals("intern")){
                gaji = 750000;
            }
            if (posisi.equals("programmer")){
                gaji = 4000000;
            }
        }
        return gaji;  
    }
    
    @Override
    public String toString(){
        return "Nama: "+nama+"\n"+
               "Divisi: "+divisi+"\n"+
               "Posisi: "+posisi+"\n"+
               "Gaji: Rp "+this.getSalary()+"\n"+
               "=============================";
    }
}