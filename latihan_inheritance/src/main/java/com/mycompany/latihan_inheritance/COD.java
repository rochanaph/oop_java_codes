/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan_inheritance;
import java.time.LocalDate;

public class COD {
    String acctNumber;
    double balance;
    LocalDate expiry;
    
    public COD (String acctNumber, double balance, LocalDate expiry){
        this.acctNumber = acctNumber;
        this.balance = balance;
        this.expiry = expiry;
    }
    
    public String toString(){
        return "acctNumber: "+acctNumber+"\n"+
                "balance: "+balance+"\n"+
                "limit: "+expiry+"\n";
    }
}
