/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.latihan_inheritance;
import java.time.LocalDate;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        // ONE Class BankAccount
//        // harus define nilai untuk semua field
        LocalDate tanggal1 = LocalDate.now();
        LocalDate tanggal2 = LocalDate.of( 2020 , 1 , 23 );

        BankAccount akunCheck = new BankAccount("123-123", 999, 9000, 0, tanggal1);
        BankAccount akunSaving = new BankAccount("333-111", 500, 0, 3, tanggal1);
        BankAccount akunCOD = new BankAccount("975-579", 999, 0, 0, tanggal2);
        System.out.println("Hasil pembuatan object dengan SATU class BankAccount");
        System.out.println("akunCheck field \n"+akunCheck);
        System.out.println("akunSaving field \n"+akunSaving);
        System.out.println("akunCOD field \n"+akunCOD);
        
        // A Class EACH
        Checking akunCheck2 = new Checking("123-123", 999, 9000);
        Savings akunSaving2 = new Savings("333-111", 500, 3);
        COD akunCOD2 = new COD("975-579", 12000, tanggal2);
        System.out.println("Hasil pembuatan object dengan TIGA class account");
        System.out.println("akunCheck field \n"+akunCheck2);
        System.out.println("akunSaving field \n"+akunSaving2);
        System.out.println("akunCOD field \n"+akunCOD2);
        
         
         // After adding extends
         CheckingExtend akunCheking3 = new CheckingExtend("123-123", 999, 9000);
         SavingsExtend akunSaving3 = new SavingsExtend("333-111", 500, 3);
         CODExtend akunCOD3 = new CODExtend("975-579", 12000, tanggal2);
         System.out.println("Hasil pembuatan object dengan Parent dan Child");
         System.out.println("akunCheck field \n"+akunCheking3);
         System.out.println("akunSaving field \n"+akunSaving3);
         System.out.println("akunCOD field \n"+akunCOD3);

    }
    
}
