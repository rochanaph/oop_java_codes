/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan_inheritance;

public class SavingsExtend extends BankAccountParent{
    int transfers;
    
    public SavingsExtend(String acctNumber, double balance, int transfers){
        this.acctNumber = acctNumber;
        this.balance = balance;
        this.transfers = transfers;
    }
    
    @Override
    public String toString(){
        return "acctNumber: "+acctNumber+"\n"+
                "balance: "+balance+"\n"+
                "transfers: "+transfers+"\n";
    }
    
}
