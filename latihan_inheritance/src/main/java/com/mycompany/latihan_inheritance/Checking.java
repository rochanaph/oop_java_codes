/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan_inheritance;

public class Checking{
    String acctNumber;
    double balance;
    double limit;
    
    public Checking (String acctNumber, double balance, double limit){
        this.acctNumber = acctNumber;
        this.balance = balance;
        this.limit = limit;
    }
    
    
    public String toString(){
        return "acctNumber: "+acctNumber+"\n"+
                "balance: "+balance+"\n"+
                "limit: "+limit+"\n";
    }
    
}
