/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan_inheritance;

public class Savings {
    String acctNumber;
    double balance;
    int transfers;
    
    public Savings(String acctNumber, double balance, int transfers){
        this.acctNumber = acctNumber;
        this.balance = balance;
        this.transfers = transfers;
    }
    
    public String toString(){
        return "acctNumber: "+acctNumber+"\n"+
                "balance: "+balance+"\n"+
                "transfers: "+transfers+"\n";
    }
}
