/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan_inheritance;

public class BankAccountParent {
    String acctNumber;
    double balance;
    
    public BankAccountParent(){
    }
    
    @Override
    public String toString(){
       return "acctNumber: "+acctNumber+"\n"+
               "balance: "+balance+"\n";
    }
}
