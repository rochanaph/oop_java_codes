/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan_inheritance;
import java.time.LocalDate;

public class BankAccount {
    String acctNumber;
    double balance;
    double limit;
    int transfers;
    LocalDate expiry;
    
    public BankAccount(String acctNumber, double balance, 
            double limit, int transfers, LocalDate expiry){
       
            this.acctNumber = acctNumber;
            this.balance = balance;
            this.limit = limit;
            this.transfers = transfers;
            this.expiry = expiry;
    }
    
    public String toString(){
       return "acctNumber: "+acctNumber+"\n"+
               "balance: "+balance+"\n"+
               "limit: "+limit+"\n"+
               "transfers: "+transfers+"\n"+ 
               "expiry: "+expiry+"\n";
    }
}
