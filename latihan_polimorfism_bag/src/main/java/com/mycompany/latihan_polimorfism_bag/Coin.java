
package com.mycompany.latihan_polimorfism_bag;

public class Coin extends Item{
    int amount;
    
    public Coin(int weight, int amount){
        this.weight = weight;
        this.amount = amount;
    }
}
