
package com.mycompany.latihan_polimorfism_bag;
import java.util.ArrayList;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // membuat object bertipe Bag
        Bag tas = new Bag();
        
        // membuat object panah bertipe Crossbow
        // cek apakah panah bisa ditambah ke tas, dengan canAddItem()
        // jika bisa maka tambahkan panah, dengan addItem()
        Crossbow panah = new Crossbow(2, 10);
        if (tas.canAddItem(panah)){
            tas.addItem(panah);
        }
        System.out.println(tas);
        
        // ada item tipe baru Map
        // menambahkan beberapa object Item dan simpan ke ArrayList
        ArrayList<Item> allItem = new ArrayList<>(); // polymorphism di sini
        Key kunci = new Key(1, 99);
        Coin koin = new Coin(1, 20);
        RareItem langka = new RareItem(3, 10);
        Map peta = new Map(2, 93);
        allItem.add(panah);
        allItem.add(kunci);
        allItem.add(koin);
        allItem.add(langka);
        allItem.add(peta);
        
        // melakukan cek berat item dengan for each loop
        for(Item var: allItem){
            if (tas.canAddItem(var)){ // polymorphism di sini
                tas.addItem(var);
            }
            else{
                System.out.println("Tas penuh!!!");
            }
        }
        System.out.println(tas);
        
    }
    
}
