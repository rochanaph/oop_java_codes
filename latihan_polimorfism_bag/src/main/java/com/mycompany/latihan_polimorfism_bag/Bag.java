
package com.mycompany.latihan_polimorfism_bag;

public class Bag {
    int currentWeight;
    
    public Bag(int currentWeight){
        // currentWeight diisi dari input
        this.currentWeight = currentWeight;
    }
    public Bag(){
        // currentWeight default
        currentWeight = 0;
    }
    
    public boolean canAddItem(Item itemx){ // polymorphism di sini
        return (currentWeight + itemx.weight < 21);
    }
    
    public void addItem(Item itemx){ // polymorphism di sini
        currentWeight = currentWeight + itemx.weight;
    }
    
    @Override
    public String toString(){
        return "Berat tas sekarang: "+currentWeight+"kg";
    }
}
