
package com.mycompany.latihan_polimorfism_bag;

public class RareItem extends Item{
    int value;
    
    public RareItem(int weight, int value){
        this.weight = weight;
        this.value = value;
    }
}
