/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.tugas_class;

/**
 *
 * @author nanadoyle
 */
public class ContactsManager {
    
    Contact [] arrayofContacts;
    int numofContacts;
    
    // inisiasi awal di constructor
    ContactsManager(){
        arrayofContacts = new Contact [1000];
        numofContacts = 0;
    }
    
    // methods
    void addContact(Contact kontak){
        int n = numofContacts;
        arrayofContacts[n] = kontak;
        numofContacts = numofContacts + 1;
        
    }
    
    // dengan komparasi ==
    Contact searchContact(String nama){
        int n = this.numofContacts;
        for (int i=0; i<n; i++){
            if (this.arrayofContacts[i].nama == nama){
                return this.arrayofContacts[i];
            }
            
        }
        return null;
    }
    
//    // dengan komparasi equals()
//    Contact searchContact(String nama){
//        int n = numofContacts;
//        for (int i=0; i<n; i++){
//            if (arrayofContacts[i].nama.equals(nama) ){
//                return arrayofContacts[i];
//            }
//            
//        }
//        return null;
//    }
    
    int getNum(){
        return numofContacts;
    }
    
    
}
