/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.tugas_class;

/**
 *
 * @author nanadoyle
 */
public class Contact {
    
    String nama;
    String email;
    String telepon;
    
    Contact(String nama, String email, String telepon){
        this.nama = nama;
        this.email = email;
        this.telepon = telepon;
    }
    
}
