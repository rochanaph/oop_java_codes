/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.tugas_class;

/**
 *
 * @author nanadoyle
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ContactsManager bukuKontak = new ContactsManager();
        Contact kontak1 = new Contact("rian","rian.adam@gmail.com","0853xxx");
        Contact kontak2 = new Contact("rochana","rochanaph@gmail.com","0812xxx");
        
        bukuKontak.addContact(kontak1);
        bukuKontak.addContact(kontak2);
        System.out.println("Jumlah kontak: "+bukuKontak.getNum());
        
        Contact hasilcari = bukuKontak.searchContact("rochana");
        System.out.println("Hasil pencarian, nama: "+hasilcari.nama);
        System.out.println("Hasil pencarian, telepon: "+hasilcari.telepon);

    }
    
}
