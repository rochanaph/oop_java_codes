/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan_encapsulation;

class Account{
    public String name;
    public String password;
    
    Account(String name, String password){
        this.name = name;
        this.password = password;
    }

}
