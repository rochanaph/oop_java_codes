/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan_encapsulation;

public class Person {
    private String userName;
    private String SSN;
    
    public Person(String userName, String SSN){
        this.userName = userName;
        this.SSN = SSN;
    }
    
    public String getUsername(){
        return userName;
    }
    private String getId(){
        return SSN + "_" + userName;
    }
    
    public boolean isSamePerson(Person p){
        if(p.getId().equals(this.getId())){
            return true;
        }
        else {
            return false;
        }
    }
    
}
