/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.latihan_encapsulation;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
//        // CEK Class Account
//        Account akun1 = new Account("rian","pass123");
//        akun1.name = "rian modif";
//        akun1.password = "passganti124";
//        System.out.println("nama modif: "+akun1.name);
//        System.out.println("password modif: "+akun1.password);
        
//        // CEK Class Book
//        Book buku1 = new Book("little woman","duck angel");
//        buku1.borrowBook();
//        boolean status = buku1.isBookBorrowed();
//        System.out.println("Status pinjaman buku: "+status);
          
          // CEK Class Person
          Person orang1 = new Person("rian94","331xxx");
          Person orang2 = new Person("rian94","332xxx");
//          String id1 = orang1.getId();
//          String id2 = orang2.getId();
//          if (id1.equals(id2)){
//              System.out.println("kedua akun orangnya sama");
//          }
//          else{
//              System.out.println("kedua akun orangnya beda");
//          }
//          System.out.println("id1: "+id1);
//          System.out.println("id2: "+id2);
          System.out.println("Cek orang1 dan orang2 apakah sama: "+orang1.isSamePerson(orang2));
        
    }
}
