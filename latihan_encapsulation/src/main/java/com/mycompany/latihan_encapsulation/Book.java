/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan_encapsulation;

class Book {
    private String title;
    private String author;
    private boolean isBorrowed;
    
    Book(String title, String author){
        this.title = title;
        this.author = author;
    }
    
    // getter
    public boolean isBookBorrowed(){
        return isBorrowed;
    }
    
    // setter
    public void borrowBook(){
        isBorrowed = true;
    }
    public void returnBook(){
        isBorrowed = false;
    }
    
    
    
}
