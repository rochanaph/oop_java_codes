package com.mycompany.uts_soal1;


public class Uts_soal1 {

    public static void main(String[] args) {
        // Pegawai object_name = new Pegawai(nama, divisi, posisi)
        Pegawai a = new Pegawai("Rio", "Back-end","intern");
        Pegawai b = new Pegawai("Alif", "Back-end","programmer");
        Pegawai c = new Pegawai("Melani", "Front-end","intern");
        Pegawai d = new Pegawai("Lintang", "Back-end","programmer");
        Pegawai e = new Pegawai("Alya", "Front-end","intern");

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
    }
}
