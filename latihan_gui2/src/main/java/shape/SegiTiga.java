/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shape;

/**
 *
 * @author Rochana
 */
public class SegiTiga extends TwoDShape{
    
    public SegiTiga(double p, double l) {
        super(p, l); 
    }

    @Override
    public double luas(){
        return 0.5*getPanjang()*getLebar();
    }
    
    @Override
    public double keliling(){
        double a = getPanjang()*getPanjang();
        double b = getLebar()*getLebar();
        double miring = Math.sqrt(a+b);
        return getPanjang()+getLebar()+miring;
    }
    
}
