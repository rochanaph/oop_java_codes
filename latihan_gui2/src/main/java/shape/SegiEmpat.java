/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shape;

/**
 *
 * @author Rochana
 */
public class SegiEmpat extends TwoDShape{
    
    public SegiEmpat(double p, double l) {
        super(p, l); 
    }

    
    @Override
    public double luas(){
        return getPanjang()*getLebar();
    }
    
    @Override
    public double keliling(){
        return (2*getPanjang())+(2*getLebar());
    }
    
}