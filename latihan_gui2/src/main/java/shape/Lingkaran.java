/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shape;

/**
 *
 * @author Rochana
 */
public class Lingkaran extends TwoDShape{
    public Lingkaran(double p, double l) {
        super(p, l); 
    }
    
    @Override
    public double luas(){
        return 3.14*getPanjang()*getLebar();
    }
    
    @Override
    public double keliling(){
        return (2*3.14*getPanjang());
    }
}
