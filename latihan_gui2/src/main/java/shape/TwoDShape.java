/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shape;

/**
 *
 * @author Rochana
 */
public abstract class TwoDShape {
    private double panjang;
    private double lebar;
    
    TwoDShape(double p, double l){
        panjang = p;
        lebar = l;
    }
    
    abstract double luas();
    abstract double keliling();
    
    public double getPanjang() {
        return panjang;
    }

    public void setPanjang(double panjang) {
        this.panjang = panjang;
    }

    public double getLebar() {
        return lebar;
    }

    public void setLebar(double lebar) {
        this.lebar = lebar;
    }

}